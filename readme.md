This code attempts to generate quasi balanced weapons. So far it's never been sucessfull.

See the project [here](https://mousetail.gitlab.io/weapon-generator/)

# Basic structure

All the code is in `generator.js`. It contains a very long list of weapon attributes, called `modfiers`. Every modifier has the following attributes:

* `id` the internal name. Used to check equality, so should be unique.
* `name` the display name. Output is formatted as `(value) (unit) (name)`
* `negativeName` (optional) the name used if the value is negative.
* `negatieve` (optional) can be set to false to prevent this attribute being used with negative value
* `increment` the minumum value to shift this attribute by.
* `cost` the cost, in MVM cash, to shift this value by one increment. Positive or negative. The
  generator will attempt to keep the total cost 0.
* `unit` usually `%`, displayed aftrer the value.

Only the lowest cost attribute in the generated weapon will be tweaked, so if you thing your attribute is very flexible set a low increment, so the relative cost is smaller.

# How the generation works

A random number between 2 and 4 `modifiers` are selected. Half of them are positive and half are negative. The total cost is calculated. If the cost is positive the negiative attribute with the lowest cost is increased, otherwise the postive attribute with the lowest cost is increased.

This way, the total cost at the end should be close to 0.