const weaponSlots = [
    {
        id: 'ScoutPrimary',
        name: 'Scattergun for the scout',
        image: 'scattergun.png'
    },
    {
        id: 'ScoutSecondary',
        name: 'Pistol for the scout',
        image: 'pistol.png',
    },
    {
        id: 'ScoutMelee',
        name: 'Bat for the scout',
        image: 'bat.png'
    },
    {
        id: 'SoldierPrimary',
        name: 'Rocket laucher for the soldier',
        image: 'rocket.png'
    },
    {
        id: 'SoldierShotgun',
        name: 'Shotgun for the soldier',
        image: 'shotgun.png'
    },
    {
        id: 'SoldierMelee',
        name: 'Shovel for the soldier',
        image: 'shovel.png'
    },
    {
        id: 'PyroPrimary',
        name: 'Flamethrower for the pyro',
        image: 'flamethrower.png'
    },
    {
        id: 'PyroShotgun',
        name: 'Shotgun for the pyro',
        image: 'shotgun.png',
    },
    {
        id: 'PyroFlaregun',
        name: 'Flaregun for the pyro',
        image: 'flaregun.png',
    },
    {
        id: 'PyroMelee',
        name: 'Axe for the pyro',
        image: 'fireaxe.png'
    },
    {
        id: 'DemoPrimary',
        name: 'Grenade laucher for the demoman',
        image: 'grenadelauncher.png',
    },
    {
        id: 'DemoSecondary',
        name: 'Stickybomb launcher for the demoman',
        image: 'stickybomblauncher.png',
    },
    {
        id: 'DemoShield',
        name: 'Shield for the demoman',
        image: 'shield.png'
    },
    {
        id: 'DemoMelee',
        name: 'Bottle for the demoman',
        image: 'bottle.png',
    },
    {
        id: 'HeavyPrimary',
        name: 'Minigun for the heavy',
        image: 'minigun.png'
    },
    {
        id: 'HeavyShotgun',
        name: 'Shotgun for the heavy',
        image: 'shotgun.png'
    },
    {
        id: 'HeavySnack',
        name: 'Lunchbox for the heavy',
        image: 'sandwich.png'
    },
    {
        id: 'HeavyMelee',
        name: 'Fists for the heavy',
        image: 'fists.png',
    },
    {
        id: 'EngineerShotgun',
        name: 'Shotgun for the engineer',
        image: 'shotgun.png'
    },
    {
        id: 'EngineerPistol',
        name: 'Pistol for the engineer',
        image: 'pistol.png'
    },
    {
        id: 'EngineerMelee',
        name: 'Wrench for the engineer',
        image: 'wrench.png'
    },
    {
        id: 'MedicPrimary',
        name: 'Syringe gun for the medic',
        image: 'syringegun.png',
    },
    /* I'm nog going to try to generate mediguns */
    {
        id: 'MedicMelee',
        name: 'Bonesaw for the medic',
        image: 'bonesaw.png',
    },
    {
        id: 'SniperPrimary',
        name: 'Sniper rifle for the sniper',
        image: 'sniperrifle.png'
    },
    {
        id: 'SniperSecondary',
        name: 'SMG for the sniper',
        image: 'smg.png'
    },
    {
        id: 'SniperMelee',
        name: 'Kukri for the sniper',
        image: 'kukri.png'
    },
    {
        id: 'SpyPrimary',
        name: 'Revolver for the spy',
        image: 'revolver.png'
    },
    /* I'm not doing sappers either */
    {
        id: 'SpyMelee',
        name: 'Knife for the spy',
        image: 'knife.png'
    }
]

const shotguns = [
    'SoliderShotgun',
    'PyroShotgun',
    'HeavyShotgun',
    'EngineerShotgun'
]

const pistols = [
    'ScoutPistol',
    'EngineerPistol'
]

const ReloadingWeapons = [
    ...shotguns,
    ...pistols,
    'SoliderPrimary',
    'ScoutPrimary',
    'PyroFlare',
    'DemoPrimary',
    'DemoSecondary',
    'MedicPrimary',
    'SniperPrimary',
    'SpyPrimary'
]

/* Weapons that are a class's primary way to deal damage, and hava a high dps */
const HighDamageWeapons = [
    'ScoutPrimary',
    'SoldierPrimary',
    'PyroPrimary',
    'DemoPrimary',
    'DemoSecondary',
    'HeavyPrimary',
    'SpyPrimary',
]

/* Low damage weapons are cheaper to increase the damage for */
const LowDamageWeapons = [
    'ScoutPistol',
    ...shotguns,
    'PyroFlare',
    'EngineerPistol',
    'SniperSecondary',
    'MedicPrimary',
    'DemoShield',
]

const MeleeWeapons = [
    'ScoutMelee',
    'SoldierMelee',
    'PyroMelee',
    'DemoMelee',
    'HeavyMelee',
    'EngineerMelee',
    'SniperMelee',
    'MedicMelee',
    'SpyMelee',
]

const DamageDealingWeapons = [
    ...HighDamageWeapons,
    ...LowDamageWeapons,
    ...MeleeWeapons
]

const SpreadWeapons = [
    ...shotguns,
    'HeavyPrimary',
    'ScoutPrimary'
]

const FiringSpeedWeapons = [

    'ScoutPrimary',
    'ScoutSecondary',
    'SoldierPrimary',
    'SoldierShotgun',
    'PyroShotgun',
    'PyroFlaregun',
    'DemoPrimary',
    'DemoSecondary',
    'HeavyPrimary',
    'HeavyShotgun',
    'EngineerShotgun',
    'EngineerPistol',
    'MeidcPrimary',
    'SniperSecondary',
    'SpyPrimary',
]

const ProjectileWeapons = [
    'SoldierPrimary',
    'PyroFlare',
    'DemoPrimary',
    'DemoSecondary',

]

const modifiers = [
    {
        id: 'PrimaryDamage',
        name: 'Damage',
        negativeName: 'damage penalty',
        increment: 5,
        cost: 80, /* Defined in terms of MVM cash */
        unit: '%',
        for: HighDamageWeapons
    },
    {
        /* Increasing damage on less powerfull weapons is cheaper */
        id: 'SecondaryDamage',
        name: 'Damage',
        negativeName: 'damage penalty',
        increment: 5,
        cost: 40,
        unit: '%',
        for: LowDamageWeapons
    },
    {
        id: 'Reload',
        name: 'Reload speed',
        increment: 15,
        cost: 40,
        unit: '%',
        for: ReloadingWeapons
    },
    {
        id: 'ClipSize',
        name: 'clip size',
        increment: 15,
        cost: 60,
        unit: '%',
        for: ReloadingWeapons
    },
    {
        id: 'deploy',
        name: 'deploy and holster speed',
        increment: 25,
        cost: 20,
        unit: '%',
        for: weaponSlots.map(
            i => i.id
        )
    },
    {
        id: 'healthOnHit',
        name: 'of damage healed on hit',
        increment: 25,
        unit: '%',
        cost: 80,
        negative: false,
        for: DamageDealingWeapons
    },
    {
        id: 'maxOverheal',
        name: 'max overheal',
        increment: 10,
        unit: '%',
        cost: 40,
        for: weaponSlots.map(
            i => i.id
        )
    },
    {
        id: 'healthTeammatesOnKill',
        name: 'health to nearby teammates on kill',
        increment: 15,
        unit: '%',
        cost: 40,
        negative: false,
        for: MeleeWeapons
    },
    {
        id: 'slowEnemyOnKill',
        name: 'slow time on target',
        increment: 10,
        unit: ' seconds',
        cost: 40,
        negative: false,
        for: DamageDealingWeapons
    },
    {
        id: 'bulletsPerShot',
        name: 'bullets per shot',
        increment: 30,
        unit: '%',
        cost: 20,
        for: SpreadWeapons
    },
    {
        id: 'healthFromHealers',
        name: 'health from healers on wearer',
        increment: 30,
        cost: 20,
        unit: '%',
        for: weaponSlots.map(
            i => i.id
        )
    },
    {
        id: 'healthFromPacks',
        name: 'health from packs on wearer',
        increment: 20,
        unit: '%',
        cost: 20,
        for: weaponSlots.map(
            i => i.id
        )
    },
    {
        id: 'ammoRegen',
        name: 'ammo regenerated every 5 seconds on wearer',
        negativeName: 'ammo drained every 5 seconds on wearer',
        increment: 1,
        unit: '',
        cost: 30,
        for: ReloadingWeapons
    },
    {
        id: 'metalRegen',
        name: 'metal regenerated every 5 seconds on wearer',
        negativeName: 'metal drained every 5 seconds on wearer',
        increment: 1,
        unit: '',
        cost: 10,
        for: [
            'EngineerPrimary',
            'EngineerPistol',
            'EngineerMelee'
        ]
    },
    {
        id: 'metalDiscount',
        name: 'metal reduction in building cost',
        increment: -15,
        unit: '%',
        cost: 40,
        for: [
            'EngineerPrimary',
            'EngineerPistol',
            'EngineerMelee',
        ]
    },
    {
        id: 'flameDistance',
        name: 'more flame range',
        negativeName: 'less flame range',
        for: [
            'PyroPrimary'
        ],
        unit: '%',
        cost: 30,
    },
    {
        id: 'tauntSpeed',
        name: 'taunt speed on wearer',
        for: MeleeWeapons,
        cost: 1,
        increment: 1,
        unit: '%',
    },
    {
        id: 'critsOnKill',
        name: 'crits on kill',
        negative: false,
        unit: ' seconds',
        increment: 10,
        cost: 40,
        for: MeleeWeapons
    },
    {
        id: 'accuracy',
        name: 'accuracy',
        unit: '%',
        increment: 15,
        cost: 20,
        for: SpreadWeapons
    },
    {
        id: 'firingSpeed',
        name: 'firing speed',
        unit: '%',
        increment: 15,
        cost: 40,
        for: FiringSpeedWeapons
    },
    {
        id: 'meleeFiringSpeed',
        name: 'swing speed',
        unit: '%',
        increment: 30,
        cost: 40,
        for: MeleeWeapons
    },
    {
        id: 'jumpHeight',
        name: ' jump height while deployed',
        unit: '%',
        increment: 25,
        cost: 40,
        for: [
            ...MeleeWeapons,
            'DemoShield',
        ]
    },
    {
        id: 'ammoCapacity',
        name: 'max ammo',
        unit: '%',
        increment: 20,
        cost: 40,
        for: [
            ...ReloadingWeapons,
            'PyroPrimary'
        ]
    },
    {
        id: 'fireResistence',
        name: ' fire resistence on wearer',
        negativeName: 'fire vulnerability on wearer',
        unit: '%',
        increment: 25,
        cost: 40,
        for: DamageDealingWeapons
    },
    {
        id: 'blastResistence',
        name: ' blast damage resistence on wearer',
        negativeName: 'blast damage vulnerability on wearer',
        unit: '%',
        increment: 25,
        cost: 80,
        for: DamageDealingWeapons
    },
    {
        id: 'bulletResistence',
        name: ' bullet damage resistence on wearer',
        negativeName: 'bullet damage vulnerability on wearer',
        unit: '%',
        increment: 25,
        cost: 80,
        for: DamageDealingWeapons
    },
    {
        id: 'critResistence',
        name: ' crit damage resistence on wearer',
        negativeName: 'crit damage vulnerability on wearer',
        unit: '%',
        increment: 25,
        cost: 80,
        for: DamageDealingWeapons
    },
    {
        id: 'movementSpeed',
        name: ' movement speed while deployed',
        unit: '%',
        increment: 25,
        cost: 40,
        for: MeleeWeapons
    },
    {
        id: 'capSpeed',
        name: ' capture speed on wearer',
        unit: '%',
        increment: 100,
        cost: 80,
        for: MeleeWeapons
    },
    {
        id: 'buildingHealth',
        name: 'building health',
        unit: '%',
        increment: 25,
        cost: 80,
        for: [
            'EngineerMelee'
        ]
    },
    {
        id: 'buildingDeploySpeed',
        name: 'bulding deploy speed',
        unit: '%',
        increment: 25,
        cost: 40,
        for: [
            'EngineerMelee',
        ]
    },
    {
        id: 'speedOnKill',
        name: 'speed boost on kill',
        unit: ' seconds',
        increment: 10,
        cost: 40,
        negative: false,
        for: MeleeWeapons
    },
    {
        id: 'speedOnHit',
        name: 'speed boost on hit',
        unit: ' seconds',
        increment: 5,
        cost: 60,
        negative: false,
        for: MeleeWeapons
    },
    {
        id: 'bonusDamageFire',
        name: 'damage boost to enemies on fire',
        unit: '%',
        increment: 25,
        negative: false,
        cost: 40,
        for: DamageDealingWeapons
    },
    {
        id: 'bonusDamageFlying',
        name: 'damage boost to airborne enemies',
        unit: '%',
        increment: 25,
        negative: false,
        cost: 40,
        for: DamageDealingWeapons
    },
    {
        id: 'bonusDamageWet',
        name: 'damage boost to wet enemies',
        unit: '%',
        negative: false,
        increment: 25,
        cost: 20,
        for: DamageDealingWeapons
    },
    {
        id: 'bonusDamageBuildings',
        name: 'extra damage versus buildings',
        negativeName: 'damage penatly versus buildings',
        increment: 25,
        cost: 40,
        unit: '%',
        for: DamageDealingWeapons
    },
    {
        id: 'bonusDamageBehind',
        name: 'damage boost when hitting enemies from behind at close range',
        negativeName: 'damage penalty when hitting enemies from behind',
        unit: '%',
        increment: 25,
        negative: false,
        cost: 60,
        for: DamageDealingWeapons
    },
    {
        id: 'afterburnDamage',
        name: 'afterburn damage bonus',
        negativeName: 'afterburn damage penalty',
        unit: '%',
        increment: 25,
        cost: 30,
        for: ['PyroPrimary, PyroFlare']
    },
    {
        id: 'afterburnTime',
        name: 'afterburn time',
        unit: '%',
        increment: 25,
        cost: 15,
        for: ['PyroPrimary', 'PyroFlare']
    },
    {
        id: 'selfDamage',
        name: ' self damage resistance on wearer',
        negativeName: 'self damage vulnerability on wearer',
        unit: '%',
        increment: 25,
        cost: 30,
        for: ['SoldierPrimary', 'DemoPrimary', 'DemoSecondary']
    },
    {
        id: 'maxHealth',
        name: 'max health on wearer',
        unit: '%',
        increment: 15,
        cost: 60,
        for: weaponSlots.map(
            i => i.id
        )
    },
    {
        id: 'projectileSpeed',
        name: 'projectile speed',
        unit: '%',
        increment: 25,
        cost: 30,
        for: ProjectileWeapons
    },
    {
        id: 'blastRadius',
        name: 'blast radius',
        unit: '%',
        cost: 30,
        increment: 25,
        for: [
            'SoldierPrimary',
            'DemoPrimary',
            'DemoSecondary'
        ]
    },
    {
        id: 'lowHealthDamage',
        name: 'damage when at <50 health',
        unit: '%',
        negative: false,
        cost: 30,
        increment: 25,
        for: DamageDealingWeapons
    },
    {
        id: 'airblastForce',
        name: 'airblast force',
        unit: '%',
        cost: 25,
        increment: 50,
        for: ['PyroPrimary'],
    },
    {
        id: 'knockback',
        name: 'knockback',
        unit: '%',
        cost: 25,
        increment: 25,
        for: [
            'SoldierPrimary',
            'DemoPromary',
            'DemoSecondary'
        ]
    },
    {
        id: 'maxStickies',
        name: 'maximum stickies out',
        unit: '',
        increment: 2,
        cost: 30,
        for: [
            'DemoSecondary'
        ]
    },
    {
        id: 'reloadOnKill',
        name: 'of clip instantly reloads on kill',
        unit: '%',
        negative: false,
        for: ReloadingWeapons,
        cost: 60,
        increment: 50
    },
    {
        id: 'spinUpTime',
        name: 'wind up speed',
        unit: '%',
        for: ['HeavyPrimary'],
        cost: 30,
        increment: 25
    },
    {
        id: 'snackHealth',
        name: 'health from eating',
        unit: '%',
        for: ['HeavySnack'],
        cost: 30,
        increment: 25
    },
    {
        id: 'snackTime',
        name: 'snack regeneration time',
        unit: '%',
        for: ['HeavySnack'],
        cost: 30,
        increment: -25
    },
    {
        id: 'healthRegen',
        name: 'health regeneration per second on wearer',
        negativeName: 'health drained per second on wearer when over 50% HP',
        for: [...DamageDealingWeapons,
            'DemoShield',
            'HeavySnack'],
        cost: 60,
        increment: 5,
        unit: ''
    },
    {
        id: 'buldingFiringSpeed',
        name: 'building firing speed',
        for: ['EngineerPrimary', 'EngineerPistol', 'EngineerMelee'],
        increment: 25,
        cost: 60,
        unit: '%'
    },
    {
        id: 'bleed',
        name: 'of bleed on enemies after hit',
        unit: ' seconds',
        increment: 10,
        cost: 30,
        negative: false,
        for: MeleeWeapons
    },
    {
        id: 'cloakTime',
        name: 'cloak time',
        unit: '%',
        increment: -25,
        cost: 25,
        for: [
            'SpyMelee'
        ]
    },
    {
        id: 'decloakTime',
        name: 'decloak time',
        unit: '%',
        increment: -25,
        cost: 25,
        for: [
            'SpyMelee'
        ]
    },
    {
        id: 'cloakDuration',
        name: 'cloak duration',
        unit: '%',
        increment: 20,
        cost: 30,
        for: [
            'SpyPrimary',
            'SpyMelee'
        ]
    },
    {
        id: 'chargeTime',
        name: 'charge time',
        unit: '%',
        increment: -25,
        cost: 40,
        for: [
            'SniperPrimary'
        ]
    },
    {
        id: 'overhealDecal',
        name: 'Overheal does not decay',
        cost: 300,
        for: weaponSlots.map(i => i.id)
    },
    {
        id: 'burningCrit',
        name: '100% critical hit vs burning players',
        cost: 300,
        for: DamageDealingWeapons
    },
    {
        id: 'wetCrit',
        name: '100% critical hit vs wet players',
        cost: 200,
        for: DamageDealingWeapons
    },
    {
        id: 'flyingCrit',
        name: '100% critical hit vs airborne enemies',
        cost: 200,
        for: DamageDealingWeapons
    },
    {
        id: 'noCompressionBlast',
        name: 'No compression blast',
        cost: -200,
        for: 'PyroPrimary'
    },
    {
        id: 'damageResistanceTeammated',
        name: '25% Damage resistance to nearby teammates while deployed',
        cost: 300,
        for: MeleeWeapons
    },
    {
        id: 'noReloadSingle',
        name: 'This weapon reloads its entire clip at once',
        cost: 100,
        for: ReloadingWeapons
    },
    {
        id: 'healthRegen',
        name: 'Health regenerated per second on wearer',
        negativeName: 'Health drained per second on wearer',
        increment: 5,
        unit: '',
        for: weaponSlots.map(i => i.id)
    },
    {
        id: 'damageFalloff',
        name: 'No Damage Falloff over distance',
        for: DamageDealingWeapons,
        cost: 200
    },
    {
        id: 'stickyBombArmTime',
        name: 'Sticky bomb arm time',
        increment: -25,
        cost: 30,
        for: 'DemoSecondary'
    },
    {
        id: 'projectileBounce',
        name: 'Projectiles bounce on surfaces with steep angles',
        cost: 1,
        for: 'SolderPrimary'
    }
]


function choice(l) {
    return l[Math.floor(l.length * Math.random())]
}

function camelToNormalCase(str) {
    return str.replace(/[A-Z]/g, (k) => ' ' + k);
}

function getTotalStrengh(...statGroups) {
    return statGroups.reduce(
        (i, group) => i + group.reduce(
            (j, stat) => j + stat.multiplyer * stat.stat.cost,
            0
        ),
        0
    )
}

function is_positive_stat(stat, neglen, poslen) {
    if (!Object.hasOwnProperty.call(stat, 'increment')) {
        return stat.cost >= 0
    } else if (Object.hasOwnProperty.call(stat, 'negative')) {
        return true
    }

    return neglen <= poslen
}

function getStats(possibleStats) {
    console.log("calling getStats()");
    const stats = [];
    const nrof_stats = Math.floor(Math.random() * 3 + 2);

    if (possibleStats.length < nrof_stats) {
        console.log("Aborting due to insufficient stats")
        return [];
    }

    for (let i = 0; i < nrof_stats; i++) {
        let new_stat = choice(possibleStats);
        while (stats.indexOf(new_stat) !== -1) {
            new_stat = choice(possibleStats)
        }
        stats.push(new_stat);
    }

    stats.sort((i, j) => j.cost - i.cost);
    console.log(stats);
    const positiveStats = [];
    const negativeStats = [];
    stats.forEach((i, index) => {
        if (is_positive_stat(i, positiveStats.length, negativeStats.length)
        ) {
            positiveStats.push({stat: i, multiplyer: 1, positive: true})
        } else {
            negativeStats.push({stat: i, multiplyer: -1, positive: false})
        }
    });

    if (negativeStats.length === 0) {
        console.log("Aborting due to no negative stats", positiveStats)
        return [];
    }

    const strengh = getTotalStrengh(positiveStats, negativeStats);
    if (strengh < -20) {
        positiveStats[positiveStats.length - 1].multiplyer -= Math.floor(
            strengh / positiveStats[positiveStats.length - 1].stat.cost
        )
    } else if (strengh > 20) {
        negativeStats[negativeStats.length - 1].multiplyer -= Math.floor(
            strengh / negativeStats[negativeStats.length - 1].stat.cost)
    }

    return [...positiveStats, ...negativeStats].filter(
        i => i.multiplyer !== 0
    );
}

document.getElementById('regenerate-button').addEventListener('click', () => {
    const classFilter = document.getElementById('class-select').value;
    const slotFilter = document.getElementById('slot-select').value;

    const slotChoices = weaponSlots.filter(
        slot => slot.id.indexOf(classFilter) !== -1 && slot.id.indexOf(slotFilter) !== -1
    )
    if (slotChoices.length === 0) {
        /* Todo: handle this better */
        return;
    }

    const slot = choice(slotChoices)

    const level = Math.floor(Math.random() * 25)
    document.querySelector('.subtitle-line').textContent = `Level ${level} ${slot.name}`
    document.querySelector('.weapon-stats img').src = 'images/' + slot.image;

    const availableStats = modifiers.filter(
        i => i.for.indexOf(slot.id) !== -1
    )


    let finalStats = [];
    do {
        finalStats = getStats(availableStats)
    } while (finalStats.length === 0)

    const finalDiv = document.querySelector('.weapon-stats-stats');
    while (finalDiv.firstChild) {
        finalDiv.removeChild(finalDiv.firstChild);
    }

    finalStats.forEach(
        (stat) => {
            const div = document.createElement('div');
            let name = stat.stat.name;
            let multiplyer = stat.multiplyer;
            if (stat.positive) {
                div.classList.add('stat-positive');
            } else {
                div.classList.add('stat-negative');
                if (stat.stat.hasOwnProperty('negativeName')) {
                    name = stat.stat.negativeName;
                    multiplyer = -stat.multiplyer;
                }
            }
            if (stat.stat.increment) {
                div.textContent = `${(stat.stat.increment * multiplyer).toLocaleString('en-US', {
                    maximumFractionDigits: 3,
                    signDisplay: 'always',
                })}${stat.stat.unit} ${name}`
            } else {
                div.textContent = `${name}`
            }
            finalDiv.appendChild(div);
        }
    )

})
